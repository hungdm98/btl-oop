package create.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import model.Time;

public class CreateTime {
	public static Time getRandomTime(ArrayList<String> listTimmeName, Map<String, String> mapTimeDescription, Set<String> specialDateSet)
	{
		Random rd = new Random();
		String name = listTimmeName.get(rd.nextInt(listTimmeName.size()));
//		String description = listTimeDescription.get(rd.nextInt(listTimeDescription.size()));
		Date date = RandomDate.getDateRandom();
		
		
//		Month cộng thêm 1 vì month được đếm từ 0
		int month = date.getMonth()+1;
		int dayOfMonth = date.getDate();

		String key = null;
		key = name.valueOf(dayOfMonth) + "/" + name.valueOf(month);
		
		String description = null;
		
		if (specialDateSet.contains(key)) {
			description = mapTimeDescription.get(key);
		}
		else {
			description = "Một ngày bình thường"; 
		}
		String link = "http://shadow.org/linkTime" + rd.nextInt(listTimmeName.size());
		Time time = new Time(name, description, link, date);
		return time;
	}
	
	
}
